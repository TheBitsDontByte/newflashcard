/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
public class UserDaoSqlImpl implements UserDao {

    JdbcTemplate jdbcTemplate;

    public UserDaoSqlImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_ADD_NEW_USER
            = "insert into Users (user_name, user_password, num_new_cards, random_order) values (?, ?, ?, ?)";

    private static final String SQL_GET_USER_BY_NAME
            = "select * from Users where user_name = ?";

    private static final String SQL_GET_USER_BY_ID
            = "select * from Users where user_id = ?";

    private static final String SQL_DELETE_USER_BY_NAME
            = "delete from Users where user_name = ?";

    private static final String SQL_DELETE_USER_BY_ID
            = "delete from Users where user_id = ?";

    private static final String SQL_GET_ALL_USERS
            = "select * from Users";

    private static final String SQL_UPDATE_USER
            = "update Users set user_name = ?, user_password = ?, num_new_cards = ?, random_order = ? where user_id = ?";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public User addNewUser(User user) {
        jdbcTemplate.update(SQL_ADD_NEW_USER,
                user.getUserName(),
                user.getUserPassword(),
                user.getNumOfNewReviewCards(),
                user.isReviewOrderRandom()
        );

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        user.setUserId(id);

        return user;
    }

    @Override
    public void updateUserInfo(User user) {
        jdbcTemplate.update(SQL_UPDATE_USER,
                user.getUserName(),
                user.getUserPassword(),
                user.getNumOfNewReviewCards(),
                user.isReviewOrderRandom(),
                user.getUserId()
        );
    }

    @Override
    public User getUserById(Integer userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_USER_BY_ID, new UserMapper(), userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public User getUserByName(String userName) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_USER_BY_NAME, new UserMapper(), userName);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void deleteUser(Integer userId) {
        jdbcTemplate.update(SQL_DELETE_USER_BY_ID, userId);
    }

    @Override
    public void deleteUser(String userName) {
        jdbcTemplate.update(SQL_DELETE_USER_BY_NAME, userName);
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SQL_GET_ALL_USERS, new UserMapper());
    }

    private class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setUserId(rs.getInt("user_id"));
            user.setUserName(rs.getString("user_name"));
            user.setUserPassword(rs.getString("user_password"));
            user.setNumOfNewReviewCards(rs.getInt("num_new_cards"));
            user.setReviewOrderRandom(rs.getBoolean("random_order"));
            return user;
        }

    }

}
