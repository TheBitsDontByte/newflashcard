/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.Card;
import java.util.List;

/**
 *
 * @author chris
 */
public interface UserCardDao {

    public Card addNewCardForUser(Card card);

    public Card getCardForUser(Integer cardId, Integer userId);

    public void updateStudyTimes(Card card);

    public void deleteCardForUser(Integer cardId, Integer userId);

    public List<Card> getAllCardsForUser(Integer userId);

    //public List<Card> getAllCardsForTag(String tag);
    //Methods specific for the app, non-crud
    public Integer getNumOfNewCards(Integer userId);

    public Integer getNumOfReviewCards(Integer userId);

    public Card getNextNewCardInOrder(Integer userId);

    public Card getNextNewCardRandom(Integer userId);

    public Card getNextReviewCard(Integer userId);

    public List<Card> getRecentCards(Integer numOfCards, Integer userId);

}
