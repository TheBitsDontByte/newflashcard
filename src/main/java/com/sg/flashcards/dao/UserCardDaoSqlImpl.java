/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.Card;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author chris
 */
public class UserCardDaoSqlImpl implements UserCardDao {

    JdbcTemplate jdbcTemplate;

    public UserCardDaoSqlImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_ADD_CARD_FOR_USER
            = "insert into user_card_bridge (user_id, card_id, added_date, last_studied_date, next_study_date) "
            + " values (?, ?, ?, ?, ?)";

    private static final String SQL_GET_CARD_FOR_USER
            = "select c.card_id, c.question, c.answer, c.tag, ucb.user_id, ucb.added_date, ucb.last_studied_date, ucb.next_study_date "
            + " from Cards c inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.card_id = ? and ucb.user_id = ?";

    private static final String SQL_UPDATE_STUDY_TIMES
            = "update user_card_bridge set last_studied_date = ?, next_study_date = ? where user_id = ? and card_id = ?";

    private static final String SQL_DELETE_CARD_FOR_USER
            = "delete from user_card_bridge where user_id = ? and card_id = ?";

    private static final String SQL_GET_ALL_CARDS_FOR_USER
            = "select c.card_id, c.question, c.answer, c.tag, ucb.user_id, ucb.added_date, ucb.last_studied_date, ucb.next_study_date "
            + " from Cards c inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.user_id = ?";

    private static final String SQL_GET_NUM_NEw_CARDS
            = "select count(*) from user_card_bridge where last_studied_date is null and user_id = ?";

    private static final String SQL_GET_NUM_REVIEW_CARDS
            = "select count(last_studied_date) from user_card_bridge where user_id = ?";

    private static final String SQL_GET_NEXT_NEW_CARD_ORDER
            = "select c.card_id, c.question, c.answer, c.tag, ucb.user_id, ucb.added_date, ucb.last_studied_date, ucb.next_study_date "
            + " from Cards c inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.user_id = ? and ucb.last_studied_date is null order by ucb.added_date, c.card_id limit 1 ";

    private static final String SQL_GET_NEXT_NEW_CARD_RANDOM
            = "select c.card_id, c.question, c.answer, c.tag, ucb.user_id, ucb.added_date, ucb.last_studied_date, ucb.next_study_date "
            + " from Cards c inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.user_id = ? and ucb.last_studied_date is null order by rand() limit 1 ";

    private static final String SQL_GET_NEXT_REVIEW_CARD
            = "select c.card_id, c.question, c.answer, c.tag, ucb.user_id, ucb.added_date, ucb.last_studied_date, ucb.next_study_date "
            + " from Cards c inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.user_id = ? and ucb.last_studied_date is not null order by next_study_date limit 1 ";

    private static final String SQL_GET_RECENT_CARDS_FOR_USER
            = "select c.card_id, c.question, c.answer, c.tag, ucb.user_id, ucb.added_date, ucb.last_studied_date, ucb.next_study_date "
            + " from Cards c inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.user_id = ? order by added_date limit ? ";

    @Override
    public Card addNewCardForUser(Card card) {

        String last = card.getLastStudiedDate() == null ? null : card.getLastStudiedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss"));
        String next = card.getNextStudyDate() == null ? null : card.getNextStudyDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss"));

        jdbcTemplate.update(SQL_ADD_CARD_FOR_USER,
                card.getUserId(),
                card.getCardId(),
                card.getAddedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss")),
                last,
                next
        );

        return card;
    }

    @Override
    public Card getCardForUser(Integer cardId, Integer userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_CARD_FOR_USER, new CardMapper(), cardId, userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void updateStudyTimes(Card card) {
        String last = card.getLastStudiedDate() == null ? null : card.getLastStudiedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss"));
        String next = card.getNextStudyDate() == null ? null : card.getNextStudyDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss"));

        jdbcTemplate.update(SQL_UPDATE_STUDY_TIMES,
                last,
                next,
                card.getUserId(),
                card.getCardId()
        );
    }

    @Override
    public void deleteCardForUser(Integer cardId, Integer userId) {
        jdbcTemplate.update(SQL_DELETE_CARD_FOR_USER, userId, cardId);
    }

    @Override
    public List<Card> getAllCardsForUser(Integer userId) {
        return jdbcTemplate.query(SQL_GET_ALL_CARDS_FOR_USER, new CardMapper(), userId);
    }

    @Override
    public Integer getNumOfNewCards(Integer userId) {
        return jdbcTemplate.queryForObject(SQL_GET_NUM_NEw_CARDS, Integer.class, userId);
    }

    @Override
    public Integer getNumOfReviewCards(Integer userId) {
        return jdbcTemplate.queryForObject(SQL_GET_NUM_REVIEW_CARDS, Integer.class, userId);
    }

    @Override
    public Card getNextNewCardInOrder(Integer userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_NEXT_NEW_CARD_ORDER, new CardMapper(), userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Card getNextNewCardRandom(Integer userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_NEXT_NEW_CARD_RANDOM, new CardMapper(), userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Card getNextReviewCard(Integer userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_NEXT_REVIEW_CARD, new CardMapper(), userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Card> getRecentCards(Integer numOfCards, Integer userId) {
        return jdbcTemplate.query(SQL_GET_RECENT_CARDS_FOR_USER, new CardMapper(), userId, numOfCards);
    }

    private class CardMapper implements RowMapper<Card> {

        @Override
        public Card mapRow(ResultSet rs, int i) throws SQLException {
            Card card = new Card();
            String question = rs.getString("question");
            String answer = rs.getString("answer");
            String tag = rs.getString("tag");
            int cardId = rs.getInt("card_id");
            int userId = rs.getInt("user_id");
            Timestamp add = rs.getTimestamp("added_date");
            Timestamp last = rs.getTimestamp("last_studied_date");
            Timestamp next = rs.getTimestamp("next_study_date");

            card.setQuestion(question);
            card.setAnswer(answer);
            card.setTag(tag);
            card.setCardId(cardId);
            card.setUserId(userId);
            card.setAddedDate(add.toLocalDateTime());
            card.setLastStudiedDate(last == null ? null : last.toLocalDateTime());
            card.setNextStudyDate(next == null ? null : next.toLocalDateTime());

            return card;
        }

    }

}
