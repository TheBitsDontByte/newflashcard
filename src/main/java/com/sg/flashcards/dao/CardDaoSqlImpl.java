/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.Card;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
public class CardDaoSqlImpl implements CardDao {

    JdbcTemplate jdbcTemplate;

    public CardDaoSqlImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_ADD_NEW_CARD
            = "insert into Cards (question, answer, is_default, tag) values (?, ?, ?, ?)";

    private static final String SQL_DELETE_CARD
            = "delete from Cards where card_id = ?";

    private static final String SQL_GET_CARD
            = "select * from Cards where card_id = ?";

    private static final String SQL_GET_ALL_CARDS
            = "select * from Cards";

    private static final String SQL_UPDATE_CARD
            = "update Cards set question = ?, answer = ?, tag = ?, is_default = ? "
            + " where card_id = ? ";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Card addNewCard(Card card, boolean isDefault) {
        jdbcTemplate.update(SQL_ADD_NEW_CARD,
                card.getQuestion(),
                card.getAnswer(),
                isDefault,
                card.getTag().toLowerCase()
        );

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);

        card.setCardId(id);
        return card;
    }

    @Override
    public Card getCard(Integer cardId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_CARD, new CardMapper(), cardId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void deleteCard(Integer cardId) {
        jdbcTemplate.update(SQL_DELETE_CARD, cardId);
    }

    @Override
    public void updateCard(Card card, boolean isDefault) {
        jdbcTemplate.update(SQL_UPDATE_CARD,
                card.getQuestion(),
                card.getAnswer(),
                card.getTag(),
                isDefault,
                card.getCardId()
        );
    }

    @Override
    public List<Card> getAllCards() {
        return jdbcTemplate.query(SQL_GET_ALL_CARDS, new CardMapper());
    }

    private class CardMapper implements RowMapper<Card> {

        @Override
        public Card mapRow(ResultSet rs, int i) throws SQLException {
            Card card = new Card();
            String question = rs.getString("question");
            String answer = rs.getString("answer");
            String tag = rs.getString("tag");
            int cardId = rs.getInt("card_id");

            card.setQuestion(question);
            card.setAnswer(answer);
            card.setTag(tag);
            card.setCardId(cardId);

            return card;
        }

    }

}
