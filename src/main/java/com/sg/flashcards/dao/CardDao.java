/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.Card;
import java.util.List;

/**
 *
 * @author chris
 */
public interface CardDao {

    public Card addNewCard(Card card, boolean isDefault);

    public Card getCard(Integer cardId);

    public void deleteCard(Integer cardId);

    public void updateCard(Card card, boolean isDefault);

    public List<Card> getAllCards();
}
