/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.User;
import java.util.List;

/**
 *
 * @author chris
 */
public interface UserDao {

    public User addNewUser(User user);

    public void updateUserInfo(User user);

    public User getUserById(Integer userId);

    public User getUserByName(String userName);

    public void deleteUser(Integer userId);

    public void deleteUser(String userName);

    public List<User> getAllUsers();
}
