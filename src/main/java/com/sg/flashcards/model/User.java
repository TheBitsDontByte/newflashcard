/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.model;

import java.util.Objects;

/**
 *
 * @author chris
 */
public class User {

    private Integer userId;
    private String userName;
    private String userPassword;
    private boolean reviewOrderRandom;
    private Integer numOfNewReviewCards;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public boolean isReviewOrderRandom() {
        return reviewOrderRandom;
    }

    public void setReviewOrderRandom(boolean reviewOrderRandom) {
        this.reviewOrderRandom = reviewOrderRandom;
    }

    public Integer getNumOfNewReviewCards() {
        return numOfNewReviewCards;
    }

    public void setNumOfNewReviewCards(Integer numOfNewReviewCards) {
        this.numOfNewReviewCards = numOfNewReviewCards;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.userId);
        hash = 29 * hash + Objects.hashCode(this.userName);
        hash = 29 * hash + Objects.hashCode(this.userPassword);
        hash = 29 * hash + (this.reviewOrderRandom ? 1 : 0);
        hash = 29 * hash + Objects.hashCode(this.numOfNewReviewCards);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.reviewOrderRandom != other.reviewOrderRandom) {
            return false;
        }
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        if (!Objects.equals(this.userPassword, other.userPassword)) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.numOfNewReviewCards, other.numOfNewReviewCards)) {
            return false;
        }
        return true;
    }

}
