/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.model;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author chris
 */
public class Card {

    private Integer userId;
    private Integer cardId;
    private String tag;
    private String question;
    private String answer;
    private LocalDateTime addedDate;
    private LocalDateTime lastStudiedDate;
    private LocalDateTime nextStudyDate;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public LocalDateTime getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDateTime addedDate) {
        this.addedDate = addedDate;
    }

    public LocalDateTime getLastStudiedDate() {
        return lastStudiedDate;
    }

    public void setLastStudiedDate(LocalDateTime lastStudiedDate) {
        this.lastStudiedDate = lastStudiedDate;
    }

    public LocalDateTime getNextStudyDate() {
        return nextStudyDate;
    }

    public void setNextStudyDate(LocalDateTime nextStudyDate) {
        this.nextStudyDate = nextStudyDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.userId);
        hash = 97 * hash + Objects.hashCode(this.cardId);
        hash = 97 * hash + Objects.hashCode(this.tag);
        hash = 97 * hash + Objects.hashCode(this.question);
        hash = 97 * hash + Objects.hashCode(this.answer);
        hash = 97 * hash + Objects.hashCode(this.addedDate);
        hash = 97 * hash + Objects.hashCode(this.lastStudiedDate);
        hash = 97 * hash + Objects.hashCode(this.nextStudyDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (!Objects.equals(this.tag, other.tag)) {
            return false;
        }
        if (!Objects.equals(this.question, other.question)) {
            return false;
        }
        if (!Objects.equals(this.answer, other.answer)) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.cardId, other.cardId)) {
            return false;
        }
        if (!Objects.equals(this.addedDate, other.addedDate)) {
            return false;
        }
        if (!Objects.equals(this.lastStudiedDate, other.lastStudiedDate)) {
            return false;
        }
        if (!Objects.equals(this.nextStudyDate, other.nextStudyDate)) {
            return false;
        }
        return true;
    }

}
