/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.Card;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author chris
 */
public class CardDaoTest {

    CardDao dao;
    Card card = new Card();

    public CardDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("cardDao", CardDao.class);

        List<Card> cards = dao.getAllCards();
        for (Card c : cards) {
            dao.deleteCard(c.getCardId());
        }

        card.setQuestion("Who is the cutest cat ?");
        card.setAnswer("AbbyGail");
        card.setTag("Pets");

    }

    @After
    public void tearDown() {
        List<Card> cards = dao.getAllCards();
        for (Card c : cards) {
            dao.deleteCard(c.getCardId());
        }
    }

    /**
     * Test of addNewCard method, of class CardDao.
     */
    @Test
    public void testAddGetCard() {
        card = dao.addNewCard(card, true);
        Card newCard = dao.getCard(card.getCardId());
        assertEquals(newCard.getCardId(), card.getCardId());
    }

    @Test
    public void testUpdate() {
        dao.addNewCard(card, true);
        Card newCard = dao.getCard(card.getCardId());
        newCard.setQuestion("Who?");
        dao.updateCard(newCard, true);

        Card testCard = dao.getCard(newCard.getCardId());
        assertTrue("Who?".equals(testCard.getQuestion()));

    }

}
