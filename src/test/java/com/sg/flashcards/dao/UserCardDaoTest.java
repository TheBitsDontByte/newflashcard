/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.Card;
import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author chris
 */
public class UserCardDaoTest {

    CardDao cardDao;
    UserCardDao dao;

    Card card = new Card();

    public UserCardDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        cardDao = ctx.getBean("cardDao", CardDao.class);
        dao = ctx.getBean("userCardDao", UserCardDao.class);

        card.setQuestion("What are the best animals");
        card.setAnswer("Cats");
        card.setTag("Animals");
        card.setAddedDate(LocalDateTime.now());
        card.setLastStudiedDate(LocalDateTime.now().minusDays(3));
        card.setNextStudyDate(LocalDateTime.now().plusDays(3));

        //Built into the DB
        card.setUserId(-5);

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetDelete() {
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card newCard = dao.getCardForUser(card.getCardId(), card.getUserId());
        assertNotNull(newCard);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());
    }

    @Test
    public void testEdgeCaseAddGetDelete() {
        cardDao.addNewCard(card, true);
        try {
            //nonexistent, should fail
            card.setUserId(999);
            dao.addNewCardForUser(card);
            fail("SHOULD NOT PASS HERE");
        } catch (Exception e) {

        }

        Card newCard = dao.getCardForUser(card.getCardId(), card.getUserId());
        assertNull(newCard);

        //does nothing, no "connection"
        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        //still deletes the card (shouldnt do this in real situations though)
        cardDao.deleteCard(card.getCardId());

    }

    @Test
    public void testUpdateStudy() {
        card.setLastStudiedDate(null);
        card.setNextStudyDate(null);
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        card.setLastStudiedDate(LocalDateTime.now());
        card.setNextStudyDate(LocalDateTime.now().plusDays(2));
        dao.updateStudyTimes(card);

        Card testCard = dao.getCardForUser(card.getCardId(), card.getUserId());
        assertNotNull(testCard.getNextStudyDate());
        assertNotNull(testCard.getLastStudiedDate());

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());
    }

    @Test
    public void testGetAllCards() {
        int size = dao.getAllCardsForUser(-5).size();
        assertEquals(0, size);

        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        size = dao.getAllCardsForUser(-5).size();
        assertEquals(1, size);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());

        size = dao.getAllCardsForUser(-5).size();
        assertEquals(0, size);

        cardDao.deleteCard(card.getCardId());

    }

    @Test
    public void testGetNewCount() {
        int newSize = dao.getNumOfNewCards(-5);

        assertEquals(0, newSize);

        card.setLastStudiedDate(null);
        card.setNextStudyDate(null);

        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        newSize = dao.getNumOfNewCards(-5);
        assertEquals(1, newSize);
        //Should not hit as a review card
        int reviewSize = dao.getNumOfReviewCards(-5);
        assertEquals(0, reviewSize);
        //Should not hit for a noexistent user
        newSize = dao.getNumOfNewCards(999);
        assertEquals(0, newSize);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());

    }

    @Test
    public void testGetReviewCount() {
        int reviewSize = dao.getNumOfReviewCards(-5);
        assertEquals(0, reviewSize);

        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        reviewSize = dao.getNumOfReviewCards(-5);
        assertEquals(1, reviewSize);
        //Should not hit as a new card
        int newSize = dao.getNumOfNewCards(-5);
        assertEquals(0, newSize);
        //Should not hit for a noexistent user
        reviewSize = dao.getNumOfReviewCards(999);
        assertEquals(0, reviewSize);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());

    }

    @Test
    public void testNewCardInOrder() {
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card card1 = dao.getCardForUser(card.getCardId(), card.getUserId());
        card1.setAddedDate(LocalDateTime.now().minusDays(1));
        card1.setLastStudiedDate(null);
        card1.setNextStudyDate(null);

        cardDao.addNewCard(card1, true);
        dao.addNewCardForUser(card1);

        Card card2 = dao.getCardForUser(card.getCardId(), card.getUserId());
        card2.setAddedDate(LocalDateTime.now().minusHours(3));
        card2.setLastStudiedDate(null);
        card2.setNextStudyDate(null);

        cardDao.addNewCard(card2, true);
        dao.addNewCardForUser(card2);

        Card testCard = dao.getNextNewCardInOrder(-5);
        assertEquals(card1.getCardId(), testCard.getCardId());

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        dao.deleteCardForUser(card1.getCardId(), card1.getUserId());
        dao.deleteCardForUser(card2.getCardId(), card2.getUserId());

        cardDao.deleteCard(card.getCardId());
        cardDao.deleteCard(card1.getCardId());
        cardDao.deleteCard(card2.getCardId());
    }

    @Test
    public void testNewCardEdgeCase() {
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card testCard = dao.getNextNewCardInOrder(-5);
        assertNull(testCard);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());
    }

    @Test
    public void testNewCardRandom() {
        card.setLastStudiedDate(null);
        card.setNextStudyDate(null);
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card testCard = dao.getNextNewCardRandom(-5);

        assertNotNull(testCard);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());
    }

    @Test
    public void testNewCardRandomNone() {
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card testCard = dao.getNextNewCardRandom(-5);

        assertNull(testCard);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());
    }

    @Test
    public void testNextReviewCard() {
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card testCard = dao.getNextReviewCard(-5);

        assertNotNull(testCard);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());
    }

    @Test
    public void testNextReviewCardNull() {
        card.setLastStudiedDate(null);
        card.setNextStudyDate(null);
        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        Card testCard = dao.getNextReviewCard(-5);

        assertNull(testCard);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());

    }

    @Test
    public void testGetNumOfRecentCards() {
        int size = dao.getRecentCards(5, -5).size();
        assertEquals(0, size);

        cardDao.addNewCard(card, true);
        dao.addNewCardForUser(card);

        size = dao.getRecentCards(5, -5).size();
        assertEquals(1, size);

        dao.deleteCardForUser(card.getCardId(), card.getUserId());
        cardDao.deleteCard(card.getCardId());

        size = dao.getRecentCards(5, -5).size();
        assertEquals(0, size);

    }

}
