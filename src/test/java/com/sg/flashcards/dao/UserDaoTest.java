/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcards.dao;

import com.sg.flashcards.model.User;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author chris
 */
public class UserDaoTest {

    UserDao dao;
    User user = new User();

    public UserDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("userDao", UserDao.class);

        user.setUserName("TestUser");
        user.setUserPassword("password");
        user.setNumOfNewReviewCards(20);
        user.setReviewOrderRandom(true);

        List<User> users = dao.getAllUsers();
        for (User u : users) {
            if (u.getUserId() > 0) {
                dao.deleteUser(u.getUserId());
            }
        }

    }

    @After
    public void tearDown() {
        List<User> users = dao.getAllUsers();
        for (User u : users) {
            if (u.getUserId() > 0) {
                dao.deleteUser(u.getUserId());
            }
        }
    }

    @Test
    public void testAddGetDeleteById() {
        dao.addNewUser(user);
        assertTrue(user.getUserId() > 0);

        User newUser = dao.getUserById(user.getUserId());
        assertTrue(newUser.getUserName().equals("TestUser"));

        dao.deleteUser(user.getUserId());

    }

    @Test
    public void testAddGetDeleteByName() {
        dao.addNewUser(user);
        assertTrue(user.getUserId() > 0);

        User newUser = dao.getUserByName("TestUser");
        assertNotNull(newUser);

        dao.deleteUser("TestUser");
    }

    @Test
    public void testUpdateUser() {
        dao.addNewUser(user);
        User newUser = dao.getUserById(user.getUserId());

        newUser.setUserName("ChangeUserName");
        dao.updateUserInfo(newUser);

        User newUser2 = dao.getUserById(newUser.getUserId());
        assertTrue("ChangeUserName".equals(newUser2.getUserName()));

        newUser.setNumOfNewReviewCards(10);
        dao.updateUserInfo(newUser);

        newUser2 = dao.getUserById(newUser.getUserId());
        assertEquals((long) 10, (long) newUser2.getNumOfNewReviewCards());

        dao.deleteUser(newUser.getUserName());
        dao.deleteUser(user.getUserName());
    }

}
